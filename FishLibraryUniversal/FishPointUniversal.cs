﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishLibraryUniversal
{
   public class FishPointUniversal
    {
        private double[] vector;
        public double this[int index]
        {
            get
            {
                if (index < vector.Length)
                    return vector[index];
                throw new IndexOutOfRangeException();
            }
            set
            {
                if (index < vector.Length)
                    vector[index] = value;
                else
                    throw new IndexOutOfRangeException();
            }
        }
        public FishPointUniversal(params double[] coords)
        {
            vector = coords;
        }
        public FishPointUniversal(int dimensionSize)
        {
            vector = new double[dimensionSize];
        }
        public double[] GetCoords()
        {
            return vector;
        }
        public static double EuclidDistance(FishPointUniversal p1, FishPointUniversal p2)
        {
            if (p1.vector.Length != p2.vector.Length)
                throw new IndexOutOfRangeException();
            else
            {
                double sum = 0;
                for (int i = 0; i < p1.vector.Length; i++)
                    sum += Math.Pow((p2[i] - p1[i]), 2);
                return Math.Sqrt(sum);
            }
        }
        public static FishPointUniversal operator -(FishPointUniversal p1, FishPointUniversal p2)
        {
            if (p1.vector.Length != p2.vector.Length)
                throw new IndexOutOfRangeException();
            else
            {
                double[] to_return = new double[p1.vector.Length];
                for (int i = 0; i < to_return.Length; i++)
                {
                    to_return[i] = p1[i] - p2[i];
                }
                return new FishPointUniversal(to_return);
            }
        }
        public static FishPointUniversal operator +(FishPointUniversal p1, FishPointUniversal p2)
        {
            if (p1.vector.Length != p2.vector.Length)
                throw new IndexOutOfRangeException();
            else
            {
                double[] to_return = new double[p1.vector.Length];
                for (int i = 0; i < to_return.Length; i++)
                {
                    to_return[i] = p1[i] + p2[i];
                }
                return new FishPointUniversal(to_return);
            }
        }
        public static FishPointUniversal operator +(FishPointUniversal p1, double number)
        {
                double[] to_return = new double[p1.vector.Length];
                for (int i = 0; i < to_return.Length; i++)
                {
                    to_return[i] = p1[i] + number;
                }
                return new FishPointUniversal(to_return);
        }
        public static FishPointUniversal operator *(FishPointUniversal p1, double factor)
        {
                double[] to_return = new double[p1.vector.Length];
                for (int i = 0; i < to_return.Length; i++)
                {
                    to_return[i] = p1[i] * factor;
                }
                return new FishPointUniversal(to_return);
        }
        public static FishPointUniversal operator /(FishPointUniversal p1, double divider)
        {
                double[] to_return = new double[p1.vector.Length];
                for (int i = 0; i < to_return.Length; i++)
                {
                    to_return[i] = p1[i] /divider;
                }
                return new FishPointUniversal(to_return);
        }
        public bool IsInRegion(FishPointUniversal lowerBound, FishPointUniversal higherBound)
        {
            if (lowerBound.vector.Length != higherBound.vector.Length||lowerBound.vector.Length!=this.vector.Length)
                throw new IndexOutOfRangeException();
            else
            {
                for (int i=0; i<lowerBound.vector.Length; i++)
                    if (this[i]<lowerBound[i]||this[i]>higherBound[i])
                        return false;
                return true;
            }
            
        }
        public override string ToString()
        {
            string to_return = "(";
            for (int i = 0; i < this.vector.Length; i++)
                to_return += i == this.vector.GetUpperBound(0) ? this[i].ToString() + ")" : this[i].ToString() + ";";
            return to_return;
        }
    }
}
